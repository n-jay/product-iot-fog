/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.analytics.utils;

import io.entgra.iot.fog.analytics.beans.SiddhiQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final Logger log = LogManager.getLogger(Utils.class);

    public static List<SiddhiQuery> getAllQueries() throws IOException {
        List<SiddhiQuery> siddhiQueries = new ArrayList<>();
        SiddhiQuery siddhiQuery;
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(Constants.SIDDHI_QUERY_DIRECTORY))) {
            for (Path path : directoryStream) {
                siddhiQuery = new SiddhiQuery();
                siddhiQuery.setName(path.getFileName().toString());
                try {
                    siddhiQuery.setContent(new String(Files.readAllBytes(path)));
                    siddhiQueries.add(siddhiQuery);
                } catch (IOException e) {
                    log.error("Error occurred while loading Siddhi query in path: " + path.toString(), e);
                }
            }
        }
        return siddhiQueries;
    }

    public static SiddhiQuery getQuery(String qName) throws IOException {
        Path path = Paths.get(Constants.SIDDHI_QUERY_DIRECTORY + qName);
        if (Files.exists(path)) {
            SiddhiQuery siddhiQuery = new SiddhiQuery();
            siddhiQuery.setName(qName);
            siddhiQuery.setContent(new String(Files.readAllBytes(path)));
            return siddhiQuery;
        } else {
            return null;
        }
    }
}
