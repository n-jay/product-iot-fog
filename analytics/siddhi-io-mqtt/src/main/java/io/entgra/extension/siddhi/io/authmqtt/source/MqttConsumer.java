/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package io.entgra.extension.siddhi.io.authmqtt.source;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.wso2.siddhi.core.stream.input.source.SourceEventListener;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * {@code ConsumerMqtt }Handle the Mqtt consuming tasks.
 */
public class MqttConsumer {
    private static final Logger log = Logger.getLogger(MqttConsumer.class);
    public SourceEventListener sourceEventListener;
    private boolean isPaused;
    private ReentrantLock lock;
    private Condition condition;
    private Map<Integer, String> dynamicParams;

    public MqttConsumer(SourceEventListener sourceEventListener, Map<Integer, String> dynamicParams) {
        this.sourceEventListener = sourceEventListener;
        this.dynamicParams = dynamicParams;
        lock = new ReentrantLock();
        condition = lock.newCondition();
    }

    public void subscribe(String topicOption, int qosOption,
                          MqttClient client) throws MqttException {
        MqttSourceCallBack callback = new MqttSourceCallBack();
        client.setCallback(callback);
        client.subscribe(topicOption, qosOption);
    }

    /**
     * MqttCallback is called when an event is received.
     */
    public class MqttSourceCallBack implements MqttCallback {

        @Override
        public void connectionLost(Throwable throwable) {
            log.debug("MQTT connection not reachable");
        }

        @Override
        public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
            if (isPaused) {
                lock.lock();
                try {
                    while (!isPaused) {
                        condition.await();
                    }
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                } finally {
                    lock.unlock();
                }
            }
            String message = new String(mqttMessage.getPayload(), StandardCharsets.UTF_8);
            if (!dynamicParams.isEmpty()) {
                String[] topicParts = topic.split("/");
                try {
                    JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();
                    dynamicParams.forEach((k, v) -> jsonObject.addProperty("meta_" + v, topicParts[k]));
                    message = jsonObject.toString();
                } catch (Exception e) {
                    log.warn("Error occurred while appending meta attributes to payload '" + message + "'", e);
                }
            }
            sourceEventListener.onEvent(message, null);
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        }
    }

    public void pause() {
        isPaused = true;
    }

    public void resume() {
        isPaused = false;
        try {
            lock.lock();
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
