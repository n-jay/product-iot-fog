/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.interceptor;

import io.entgra.iot.fog.usermgt.authenticator.BasicAuthAuthenticator;
import io.entgra.iot.fog.usermgt.beans.AuthenticationInfo;
import io.entgra.iot.fog.usermgt.beans.AuthenticationStatus;
import io.entgra.iot.fog.usermgt.beans.AuthenticationType;
import io.entgra.iot.fog.usermgt.beans.UserCredential;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.exceptions.AuthenticationException;
import io.entgra.iot.fog.usermgt.utils.CommonUtil;
import io.entgra.iot.fog.usermgt.utils.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.msf4j.Request;
import org.wso2.msf4j.Response;
import org.wso2.msf4j.interceptor.RequestInterceptor;

import javax.ws.rs.core.HttpHeaders;

public class BasicAuthRequestInterceptor implements RequestInterceptor {

    private static final Logger log = LogManager.getLogger(BasicAuthRequestInterceptor.class);

    protected boolean authenticate(String username, String password) throws AuthenticationException {
        BasicAuthAuthenticator basicAuthAuthenticator = new BasicAuthAuthenticator();
        UserCredential userCredential = new UserCredential(username, password);

        return basicAuthAuthenticator.doAuthenticate(userCredential, AuthenticationType.USERNAME);

    }

    @Override
    public boolean interceptRequest(Request request, Response response) throws Exception {
        final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        AuthenticationInfo authenticationInfo = new AuthenticationInfo();

        if (authorization != null && authorization.startsWith(Constants.AUTH_TYPE_BASIC)) {
            UserCredential credential = CommonUtil.parseBasicAuthHeader(authorization);
            try {
                if (authenticate(credential.getUsername(), credential.getPassword())) {
                    if (log.isDebugEnabled()) {
                        log.debug("Successfully authenticated the user: " + credential.getUsername());
                    }
                    authenticationInfo
                            .setUser(UserManagementDAOFactory.getUserManagementDAO().getUser(credential.getUsername()));
                    authenticationInfo.setStatus(AuthenticationStatus.SUCCESS);
                    request.setProperty("authenticationInfo", authenticationInfo);
                    return true;
                }
            } catch (AuthenticationException e) {
                log.error("Error occurred while authenticating the user", e);
                response.setStatus(javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
                return false;
            }
        }
        response.setStatus(javax.ws.rs.core.Response.Status.UNAUTHORIZED.getStatusCode());
        response.setHeader(javax.ws.rs.core.HttpHeaders.WWW_AUTHENTICATE, Constants.AUTH_TYPE_BASIC);
        return false;
    }
}
