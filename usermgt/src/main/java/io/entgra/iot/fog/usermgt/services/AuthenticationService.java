/*
 *  Copyright (c) 2019 Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 *  Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */

package io.entgra.iot.fog.usermgt.services;

import io.entgra.iot.fog.usermgt.authenticator.BasicAuthAuthenticator;
import io.entgra.iot.fog.usermgt.beans.*;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAO;
import io.entgra.iot.fog.usermgt.dao.UserManagementDAOFactory;
import io.entgra.iot.fog.usermgt.exceptions.AuthenticationException;
import io.entgra.iot.fog.usermgt.exceptions.UserManagementException;
import io.entgra.iot.fog.usermgt.utils.CommonUtil;
import io.entgra.iot.fog.usermgt.utils.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.wso2.msf4j.Request;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("/usermgt/authenticate")
public class AuthenticationService {

    private static final Logger log = LogManager.getLogger(AuthenticationService.class);

    @POST
    @Produces("application/json")
    public Response post(@Context Request request) {
        BasicAuthAuthenticator basicAuthAuthenticator = new BasicAuthAuthenticator();
        final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        UserCredential credential;

        if (authorization != null && authorization.startsWith(Constants.AUTH_TYPE_BASIC)) {
            try {
                credential = CommonUtil.parseBasicAuthHeader(authorization);
                boolean authenticated = basicAuthAuthenticator.doAuthenticate(credential, AuthenticationType.USERNAME);
                return Response.ok(CommonUtil.getAuthenticationInfo(credential, authenticated)).build();
            } catch (AuthenticationException | UserManagementException e) {
                return Response.serverError().entity(e.getMessage()).build();
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Authorization header is incorrect. Either header does not exist or auth type is not basic.");
            }

            return Response.status(Response.Status.UNAUTHORIZED)
                    .header(HttpHeaders.WWW_AUTHENTICATE, Constants.AUTH_TYPE_BASIC).build();
        }
    }

    @POST
    @Path("/mobile")
    @Produces("application/json")
    public Response mobileAuthentication(@Context Request request) {
        BasicAuthAuthenticator basicAuthAuthenticator = new BasicAuthAuthenticator();
        final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        UserCredential credential;

        if (authorization != null && authorization.startsWith(Constants.AUTH_TYPE_BASIC)) {
            try {
                credential = CommonUtil.parseBasicAuthHeader(authorization);
                boolean authenticated = basicAuthAuthenticator.doAuthenticate(credential, AuthenticationType.MOBILE);
                //here it takes the mobile no
                if (!authenticated) {
                    User user = new User();
                    user.setUserName(credential.getUsername());
                    AuthenticationInfo authenticationInfo = new AuthenticationInfo();
                    authenticationInfo.setMessage("Failed to authorize incoming request.");
                    authenticationInfo.setUser(user);
                    authenticationInfo.setStatus(AuthenticationStatus.INVALID_PASSWORD);
                    return Response.ok(authenticationInfo).build();
                }
                String userName = UserManagementDAOFactory.getUserManagementDAO().getUserName(credential.getUsername());
                credential.setUsername(userName);
                return Response.ok(CommonUtil.getAuthenticationInfo(credential, authenticated)).build();
            } catch (AuthenticationException | UserManagementException e) {
                return Response.serverError().entity(e.getMessage()).build();
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Authorization header is incorrect. Either header does not exist or auth type is not basic.");
            }

            return Response.status(Response.Status.UNAUTHORIZED)
                    .header(HttpHeaders.WWW_AUTHENTICATE, Constants.AUTH_TYPE_BASIC).build();
        }
    }

}
